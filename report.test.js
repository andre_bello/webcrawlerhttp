const { sortPages } = require('./report.js');
const { test, expect } = require('@jest/globals');

test('sortPages 2 pages', () => {
  const input = {
    'https://wikipedia.org/wiki/anubis': 1,
    'https://wikipedia.org': 3
  }
  const actual = sortPages(input)
  const expected = [
    ['https://wikipedia.org', 3],
    ['https://wikipedia.org/wiki/anubis', 1]
  ]
  expect(actual).toEqual(expected)
})

test('sortPages 5 pages', () => {
  const input = {
    'https://wikipedia.org/wiki/anubis': 1,
    'https://wikipedia.org/wiki/anput': 3,
    'https://wikipedia.org': 4,
    'https://wikipedia.org/wiki/linux': 2,
    'https://wikipedia.org/wiki/emacs': 5
  }
  const actual = sortPages(input)
  const expected = [
    ['https://wikipedia.org/wiki/emacs', 5],
    ['https://wikipedia.org', 4],
    ['https://wikipedia.org/wiki/anput', 3],
    ['https://wikipedia.org/wiki/linux', 2],
    ['https://wikipedia.org/wiki/anubis', 1]
  ]
  expect(actual).toEqual(expected)
})

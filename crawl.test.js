const { normalizeURL, getURLsFromHTML } = require('./crawl.js');
const { test, expect } = require('@jest/globals');

test('normalizeURL strip protocol', () => {
  const input = 'https://www.wikipedia.org/wiki/anubis'
  const actual = normalizeURL(input)
  const expected = 'www.wikipedia.org/wiki/anubis'
  expect(actual).toEqual(expected)
})

test('normalizeURL strip trailing slash', () => {
  const input = 'https://www.wikipedia.org/wiki/anubis/'
  const actual = normalizeURL(input)
  const expected = 'www.wikipedia.org/wiki/anubis'
  expect(actual).toEqual(expected)
})

test('normalizeURL capitals', () => {
  const input = 'https://www.WIKIPEDIA.org/wiki/anubis/'
  const actual = normalizeURL(input)
  const expected = 'www.wikipedia.org/wiki/anubis'
  expect(actual).toEqual(expected)
})

test('normalizeURL strip http', () => {
  const input = 'http://www.wikipedia.org/wiki/anubis/'
  const actual = normalizeURL(input)
  const expected = 'www.wikipedia.org/wiki/anubis'
  expect(actual).toEqual(expected)
})

test('getURLsFromHTML absolute', () => {
  const inputHTMLBody = `
<html>
    <body>
        <a href="https:/wikipedia.org/wiki/anubis/">Wikipedia</a>
    </body>
</html>
`
  const inputBaseURL = 'https://wikipedia.org'
  const actual = getURLsFromHTML(inputHTMLBody, inputBaseURL)
  const expected = ['https://wikipedia.org/wiki/anubis/']
  expect(actual).toEqual(expected)
})

test('getURLsFromHTML relative', () => {
  const inputHTMLBody = `
<html>
    <body>
        <a href="/wiki/anubis/">Wikipedia</a>
    </body>
</html>
`
  const inputBaseURL = 'https://wikipedia.org'
  const actual = getURLsFromHTML(inputHTMLBody, inputBaseURL)
  const expected = ['https://wikipedia.org/wiki/anubis/']
  expect(actual).toEqual(expected)
})

test('getURLsFromHTML both', () => {
  const inputHTMLBody = `
<html>
    <body>
        <a href="https://wikipedia.org/wiki/anubis/">Wikipedia Anubis</a>
        <a href="/wiki/anput/">Wikipedia Anput</a>
    </body>
</html>
`
  const inputBaseURL = 'https://wikipedia.org'
  const actual = getURLsFromHTML(inputHTMLBody, inputBaseURL)
  const expected = ['https://wikipedia.org/wiki/anubis/', 'https://wikipedia.org/wiki/anput/']
  expect(actual).toEqual(expected)
})

test('getURLsFromHTML invalid url', () => {
  const inputHTMLBody = `
<html>
    <body>
        <a href="invalid">Invalid URL</a>
    </body>
</html>
`
  const inputBaseURL = 'https://wikipedia.org'
  const actual = getURLsFromHTML(inputHTMLBody, inputBaseURL)
  const expected = []
  expect(actual).toEqual(expected)
})
